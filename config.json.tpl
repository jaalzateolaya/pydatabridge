{
	"environment": "development",
	"app": {
		"db_path": "main.db"
	},
	"adapters": {
		"woocommerce": {
			"api_url": "http://localhost:8080",
			"api_key": "api_key",
			"api_secret": "api_secret",
			"wp_user": "user-with-publisher-role",
			"wp_app_password": "user's password"
		},
		"prestashop": {
			"api_url": "http://localhost:8081",
			"api_key": "api_key",
			"default_parent_category_id": "2"
		}
	}
}
