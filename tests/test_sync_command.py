"""Test module"""

from unittest.mock import Mock
from pytest import fixture

from src.pydatabridge.sync_command import SyncCommand


class TestSyncCommand:
    """Tests the synchornization command."""

    def test_execute(self, strategy, adapters):
        """Tests the command executes as expected."""
        command = SyncCommand(strategy, adapters)
        command.execute()

        strategy.sincronize.asser_called_with(adapters)

    @fixture
    def strategy(self):
        """Strategy mock.

        """
        return Mock()

    @fixture
    def adapters(self):
        """Data access adapter mocks for slaves.

        """
        return [Mock(), Mock(), Mock()]
