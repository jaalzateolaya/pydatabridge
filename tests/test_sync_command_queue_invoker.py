"""Test module."""

from unittest.mock import Mock
from pytest import fixture

from src.pydatabridge.sync_command_queue_invoker import SyncCommandQueueInvoker


class TestSyncCommandsQueueInvoker():
    """Tests the command queue invoker."""
    def test_add_and_execute_commands(self, invoker, commands):
        """Tests adding commands and executing them."""

        for command in commands:
            invoker.add(command)

        invoker.execute_commands()

        # Verify that all commands in the queue were executed
        for command in commands:
            command.execute.assert_called_once()

    @fixture
    def invoker(self):
        """Invoker fixture.

        """
        return SyncCommandQueueInvoker()

    @fixture
    def commands(self):
        """Commands mocks.

        """
        return [Mock(), Mock(), Mock()]
