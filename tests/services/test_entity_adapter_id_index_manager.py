# pylint: disable=missing-function-docstring,missing-class-docstring,missing-module-docstring

from unittest.mock import Mock
import pytest
from src.pydatabridge.services import EntityAdapterIdIndexManager


class TestEntityAdapterIdIndexManager:
    def test_save_id_for_when_new_id(
        self,
        mock_db,
        mock_logger_provider,
        sample_entity,
        sample_adapter
    ):
        manager = EntityAdapterIdIndexManager(mock_db, mock_logger_provider)
        mock_db.cursor().fetchone.return_value = ("local_id_456",)

        result = manager.save_id_for(sample_entity, "collection_name", sample_adapter)

        assert result == "local_id_456"
        mock_db.cursor().execute.assert_called()
        mock_db.cursor().connection.commit.assert_called()

    def test_save_id_for_when_existing_id(
        self,
        mock_db,
        mock_logger_provider,
        sample_entity, sample_adapter
    ):
        manager = EntityAdapterIdIndexManager(mock_db, mock_logger_provider)
        mock_db.cursor().fetchone.return_value = ("local_id_456",)

        result = manager.save_id_for(
            sample_entity,
            "collection_name",
            sample_adapter,
            _id="local_id_456"
        )

        assert result == "local_id_456"

    def test_get_local_id_for_when_found(
        self,
        mock_db,
        mock_logger_provider,
        sample_entity,
        sample_adapter
    ):
        manager = EntityAdapterIdIndexManager(mock_db, mock_logger_provider)
        mock_db.cursor().fetchone.return_value = ("local_id_456",)

        result = manager.get_local_id_for(sample_entity, "collection_name", sample_adapter)

        assert result == "local_id_456"

    def test_get_local_id_for_when_not_found(
        self,
        mock_db,
        mock_logger_provider,
        sample_entity,
        sample_adapter
    ):
        manager = EntityAdapterIdIndexManager(mock_db, mock_logger_provider)
        mock_db.cursor().fetchone.return_value = None

        result = manager.get_local_id_for(sample_entity, "collection_name", sample_adapter)

        assert result is None

    def test_get_adapter_id_for_when_found(
        self,
        mock_db,
        mock_logger_provider,
        sample_entity,
        sample_adapter
    ):
        manager = EntityAdapterIdIndexManager(mock_db, mock_logger_provider)
        mock_db.cursor().fetchone.return_value = ("local_id_456",)

        result = manager.get_adapter_id_for(sample_entity, "collection_name", sample_adapter)

        assert result == "local_id_456"

    def test_get_adapter_id_for_when_not_found(
        self,
        mock_db,
        mock_logger_provider,
        sample_entity,
        sample_adapter
    ):
        manager = EntityAdapterIdIndexManager(mock_db, mock_logger_provider)
        mock_db.cursor().fetchone.return_value = None

        result = manager.get_adapter_id_for(sample_entity, "collection_name", sample_adapter)

        assert result is None

    @pytest.fixture
    def mock_db(self):
        return Mock()

    @pytest.fixture
    def mock_logger_provider(self):
        logger = Mock()
        return lambda name: logger

    # Sample data
    @pytest.fixture
    def sample_entity(self):
        entity = Mock()
        entity.id = "entity_id_123"
        return entity

    @pytest.fixture
    def sample_adapter(self):
        adapter = Mock()
        adapter.provider_name = "adapter_name_xyz"
        return adapter
