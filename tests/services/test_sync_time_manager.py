# pylint: disable=missing-function-docstring,missing-class-docstring
# pylint: disable=protected-access,missing-module-docstring

from unittest.mock import Mock
import sqlite3
from datetime import datetime
import pytest

from src.pydatabridge.services import SyncTimeManager


class TestSyncTimeManager:
    def test_times_gets_logged(self, sync_timestamp_manager: SyncTimeManager):
        entity = 'test_entity'
        now = datetime.now()

        sync_timestamp_manager.log_for(entity, now)

        last_timestamp = sync_timestamp_manager.get_last_for(entity)

        assert last_timestamp is not None
        assert isinstance(last_timestamp, datetime)
        assert last_timestamp == now

    def test_get_last_for_when_none(self, sync_timestamp_manager: SyncTimeManager):
        entity = 'other_test_entity'

        last_timestamp = sync_timestamp_manager.get_last_for(entity)

        assert last_timestamp is None
        sync_timestamp_manager._logger.debug.assert_called_with(
            'No sync time loaded for %s',
            entity
        )

    @pytest.fixture
    def db(self):
        connection = sqlite3.connect(':memory:')
        yield connection
        connection.close()

    @pytest.fixture
    def sync_timestamp_manager(self, db):
        logger = Mock()
        return SyncTimeManager(db, logger)
