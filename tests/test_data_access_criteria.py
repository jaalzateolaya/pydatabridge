"""Test module."""

from datetime import datetime

from src.pydatabridge.data_access_criteria import DataAccessCriteria

class TestDataAccessCriteria:
    """Tests data access criteria."""

    def test_created_since_criteria_creation(self):
        """Tests factory method."""
        since = datetime.now()
        criteria = DataAccessCriteria.create_criteria_modified_or_created(since)

        assert criteria.modified_or_created_since == since
