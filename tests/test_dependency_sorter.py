"""Test module."""

from unittest.mock import Mock
import pytest

from src.pydatabridge.services.dependency_sorter import DependencySorter


class TestDependencySorter:
    """Tests dependency sorter.

    """

    def test_dependency_sorting(self, objects_with_dependencies):
        """Tests dependency sorting.

        """
        sorter = DependencySorter('entity', 'depends_on')
        sorted_objects = sorter.sort(objects_with_dependencies)

        # Verify that each object is placed after its dependency
        index_map = {obj.entity: i for i, obj in enumerate(sorted_objects)}
        for obj in objects_with_dependencies:
            if obj.depends_on:
                assert index_map[obj.depends_on] < index_map[obj.entity]

    def test_no_dependency(self, objects_without_dependencies):
        """Test sorting when no dependencies at all.

        """
        sorter = DependencySorter('entity', 'depends_on')
        sorted_objects = sorter.sort(objects_without_dependencies)

        # Since there are no dependencies, all objects should be present in the result
        sorted_entities = [obj.entity for obj in sorted_objects]
        assert set(sorted_entities) == set(obj.entity for obj in objects_without_dependencies)

    @pytest.fixture
    def objects_with_dependencies(self):
        """Fixture: Object with dependencies.

        """
        obj_a = Mock(entity='ObjA', depends_on='ObjB')
        obj_b = Mock(entity='ObjB', depends_on='ObjC')
        obj_c = Mock(entity='ObjC', depends_on=None)
        obj_d = Mock(entity='ObjD', depends_on=None)
        return [obj_a, obj_b, obj_c, obj_d]

    @pytest.fixture
    def objects_without_dependencies(self):
        """Fixture: Object without dependencies.

        """
        obj1 = Mock(entity='Obj1', depends_on=None)
        obj2 = Mock(entity='Obj2', depends_on=None)
        obj3 = Mock(entity='Obj3', depends_on=None)
        return [obj1, obj2, obj3]
