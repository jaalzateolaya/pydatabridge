# pylint: disable=missing-function-docstring,missing-class-docstring
# pylint: disable=protected-access,missing-module-docstring

from unittest.mock import Mock
from typing import Iterable, Any
from datetime import datetime
import logging
from pytest import  fixture

from src.pydatabridge.sync_strategies.data_push_strategy import DataPushStrategy
from src.pydatabridge.data_access_adapter import DataAccessAdapter
from src.pydatabridge.data_access_model import DataAccessModel
from src.pydatabridge.data_access_criteria import DataAccessCriteria
from src.pydatabridge.services import SyncTimeManager

class AnIncrementalPushingSync(DataPushStrategy):
    @property
    def entity_name(self) -> str:
        return 'an_entity'


# pylint: disable=protected-access
class TestDataPushStrategy:
    def test_default_properties(self, strategy):
        assert strategy._source_adapters is None
        assert strategy._dest_adapters is None
        assert strategy._data is None

    def test_prepare_adapters_performed_on_synchronize(
        self,
        strategy,
        mock_adapters,
        mock_source_adapter,
        mock_dest_adapters
    ):
        strategy.synchronize(mock_adapters)

        assert isinstance(strategy._source_adapters, list)
        assert strategy._source_adapters == [mock_source_adapter]

        assert isinstance(strategy._dest_adapters, list)
        assert strategy._dest_adapters == mock_dest_adapters

    def test_prepare_data_preformed_on_synchronize(
        self,
        strategy,
        mock_adapters,
        mock_source_adapter,
        mock_data
    ):
        mock_source_adapter.read.return_value = mock_data

        strategy.synchronize(mock_adapters)

        assert isinstance(strategy._data, list)
        mock_source_adapter.read.assert_called_once()
        assert strategy._data == mock_data

    def test_prepare_data_honors_source_adapter_status(
        self,
        strategy,
        mock_adapters,
        mock_source_adapter,
    ):
        mock_source_adapter.check_status.return_value = False

        strategy.synchronize(mock_adapters)

        mock_source_adapter.read.assert_not_called()
        assert len(strategy._data) == 0

    def test_loads_data_using_criteria(
        self,
        strategy,
        mock_adapters: Iterable[DataAccessAdapter],
        mock_source_adapter,
        mock_sync_time_manager,
    ):
        criteria = DataAccessCriteria.create_criteria_modified_or_created(
            since=mock_sync_time_manager.get_last_for.return_value
        )

        strategy.synchronize(mock_adapters)

        mock_source_adapter.read.assert_called_once_with(strategy.entity_name, criteria)

    def test_save_data_into_dest_adapters(
        self,
        strategy,
        mock_adapters: Iterable[DataAccessAdapter],
        mock_source_adapter,
        mock_dest_adapters,
        mock_data: Iterable[Any],
    ):
        mock_source_adapter.read.return_value = mock_data

        strategy.synchronize(mock_adapters)

        mock_source_adapter.read.assert_called_once()
        for adapter in mock_dest_adapters:
            save_mock = adapter.save
            save_mock.assert_called_once_with(strategy.entity_name, mock_data)


    def test_synchronize_no_data(
        self,
        strategy,
        mock_adapters,
        mock_logger
    ):
        strategy.synchronize(mock_adapters)

        mock_logger.info.assert_called_with('There is nothing to be synced.')

    def test_synchronize_dry_run(self, strategy, mock_logger, mock_data):
        strategy._data = mock_data
        strategy.set_dry_run_mode_is_on(True)

        strategy._synchronize()

        mock_logger.info.assert_called_with(
            'There are %d %s to be synced, disable dry-run to perform syncrhonization',
            len(mock_data),
            strategy.entity_name
        )

    @fixture
    def mock_logger(self):
        return Mock(spec=logging.Logger)

    @fixture
    def mock_sync_time_manager(self):
        mock = Mock(spec=SyncTimeManager)
        mock.get_last_for.return_value = datetime.now()
        return mock

    @fixture
    def mock_id_index_manager(self):
        attrs = {
            'get_local_id_for.side_effect': lambda item, of, _from: item.id,
            'get_adapter_id_for.side_effect': lambda item, of, _from: item.id,
        }
        return Mock(**attrs)

    @fixture
    def strategy(self, mock_logger, mock_sync_time_manager, mock_id_index_manager):
        return AnIncrementalPushingSync(
            mock_logger,
            mock_sync_time_manager,
            mock_id_index_manager
        )

    @fixture
    def mock_source_adapter(self):
        adapter = Mock(spec=DataAccessAdapter, provider_name='SourceTestAdapter', **{
            'check_status.return_value': True,
            'read.return_value': [],
            'save.side_effect': lambda entity_name, data: data,
        })
        return adapter

    @fixture
    def mock_dest_adapters(self):
        attrs = {
            'check_status.return_value': True,
            'save.side_effect': lambda entity_name, data: data,
        }

        return [
            Mock(spec=DataAccessAdapter, provider_name="'DestTestAdapter'", **attrs),
            Mock(spec=DataAccessAdapter, provider_name="'DestTestAdapter'", **attrs)
        ]

    @fixture
    def mock_adapters(self, mock_source_adapter, mock_dest_adapters):
        return [mock_source_adapter] + mock_dest_adapters

    @fixture
    def mock_data(self) -> Iterable[DataAccessModel]:
        return [
            DataAccessModel(None),
            DataAccessModel(None),
            DataAccessModel(1),
            DataAccessModel(2),
            DataAccessModel(3)
        ]
