# pylint: disable=missing-function-docstring,missing-class-docstring
# pylint: disable=protected-access,missing-module-docstring

from unittest.mock import Mock
from pytest import fixture

from src.pydatabridge.sync_strategies.data_pull_strategy import DataPullStrategy
from src.pydatabridge.data_access_adapter import DataAccessAdapter

class AnIncrementalPullingSync(DataPullStrategy):
    @property
    def entity_name(self) -> str:
        return 'an_entity'

class TestDataPullStrategy:
    def test_prepare_adapters(
        self,
        strategy,
        mock_adapters,
        mock_source_adapters,
        mock_dest_adapter
    ):
        strategy._adapters = mock_adapters
        strategy._prepare_adapters()

        assert isinstance(strategy._source_adapters, list)
        assert strategy._source_adapters == mock_source_adapters

        assert isinstance(strategy._dest_adapters, list)
        assert strategy._dest_adapters == [mock_dest_adapter]


    @fixture
    def strategy(self):
        return AnIncrementalPullingSync(Mock(), Mock(), Mock())

    @fixture
    def mock_dest_adapter(self):
        adapter = Mock(spec=DataAccessAdapter, provider_name='DestTestAdapter', **{
            'check_status.return_value': True,
            'read.return_value': [],
            'save.side_effect': lambda entity_name, data: data,
        })
        return adapter

    @fixture
    def mock_source_adapters(self):
        attrs = {
            'check_status.return_value': True,
            'save.side_effect': lambda entity_name, data: data,
        }

        return [
            Mock(spec=DataAccessAdapter, provider_name='SourceTestAdapter', **attrs),
            Mock(spec=DataAccessAdapter, provider_name='SourceTestAdapter', **attrs)
        ]

    @fixture
    def mock_adapters(self, mock_source_adapters, mock_dest_adapter):
        return [mock_dest_adapter] + mock_source_adapters
