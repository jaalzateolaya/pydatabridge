# pylint: disable=missing-function-docstring,missing-class-docstring
# pylint: disable=protected-access,missing-module-docstring

from unittest.mock import Mock, patch
from datetime import datetime
from pytest import fixture

from src.pydatabridge.sync_strategies.snapshot_strategy import SnapshotStrategy
from src.pydatabridge.services import SyncTimeManager

class ConcreteSnapshotStrategy(SnapshotStrategy):
    @property
    def entity_name(self) -> str:
        return 'an-entity'

    def _prepare(self):
        pass

    def _synchronize(self):
        pass

class TestSnapshotStrategy:
    def test_initialization(self, mock_logger, mock_sync_time_manager):
        strategy = ConcreteSnapshotStrategy(mock_logger, mock_sync_time_manager)
        assert strategy._logger is mock_logger
        assert strategy._sync_time_manager is mock_sync_time_manager
        assert isinstance(strategy._last_sync_time, datetime)

    def test_synchronize_with_insufficient_adapters(self, mock_logger, mock_sync_time_manager):
        strategy = ConcreteSnapshotStrategy(mock_logger, mock_sync_time_manager)

        strategy.synchronize([])
        mock_logger.error.assert_called_once_with('Not enough adapters to perform synchronizaton')

    def test_synchronize_normal_operation(self, strategy, mock_sync_time_manager, mock_adapters):
        with patch.object(strategy, '_prepare'), patch.object(strategy, '_synchronize'):
            strategy.synchronize(mock_adapters)

            strategy._prepare.assert_called_once()
            strategy._synchronize.assert_called_once()
            mock_sync_time_manager.log_for.assert_called_once()

    def test_save_new_last_sync_time(self, strategy, mock_sync_time_manager):
        strategy._new_last_sync_time = datetime.now()
        strategy._save_new_last_sync_time()

        mock_sync_time_manager.log_for.assert_called_once_with(
            strategy.entity_name,
            strategy._new_last_sync_time
        )

    def test_save_new_last_sync_time_when_dry_run(self, strategy, mock_sync_time_manager):
        strategy._dry_run_mode_is_on = True
        strategy._new_last_sync_time = datetime.now()
        strategy._save_new_last_sync_time()

        mock_sync_time_manager.log_for.assert_not_called()

    @fixture
    def strategy(self, mock_logger, mock_sync_time_manager):
        return ConcreteSnapshotStrategy(mock_logger, mock_sync_time_manager)

    @fixture
    def mock_logger(self):
        return Mock()

    @fixture
    def mock_sync_time_manager(self):
        mock_manager = Mock(spec=SyncTimeManager)
        mock_manager.get_last_for.return_value = datetime.now()
        return mock_manager

    @fixture
    def mock_adapters(self):
        return [Mock(), Mock()]
