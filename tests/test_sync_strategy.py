# pylint: disable=missing-function-docstring,missing-class-docstring
# pylint: disable=protected-access,missing-module-docstring

from typing import Iterable
from pytest import fixture

from src.pydatabridge.sync_strategy import SyncStrategy
from src.pydatabridge.data_access_adapter import DataAccessAdapter

class ConcreteSyncStrategy(SyncStrategy):
    @property
    def entity_name(self) -> str:
        return 'an-entity'

    def synchronize(self, adapters: Iterable[DataAccessAdapter]) -> None:
        pass

class TestSyncStrategy:
    def test_support_dry_run_is_false_by_default(self, strategy):
        assert strategy.supports_dry_run is False

    def test_depends_on_is_none_by_default(self, strategy):
        assert strategy.depends_on is None

    def test_set_dry_run_mode_is_on(self, strategy):
        strategy.set_dry_run_mode_is_on(True)

        assert strategy._dry_run_mode_is_on is True

    @fixture
    def strategy(self):
        return ConcreteSyncStrategy()
