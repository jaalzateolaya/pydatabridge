# pylint: disable=missing-function-docstring,missing-class-docstring
# pylint: disable=protected-access,missing-module-docstring

from unittest.mock import Mock, patch
from pytest import fixture, raises

from src.pydatabridge.data_access_adapter import DataAccessAdapter
from src.pydatabridge.data_access_criteria import DataAccessCriteria
from src.pydatabridge.data_access_model import DataAccessModel


class ConcreteDataAccessAdapter(DataAccessAdapter):
    @property
    def provider_name(self):
        return 'mock'

    def initialize(self):
        self._create_clients = Mock()
        self._update_clients = Mock()
        self._read_clients = Mock()
        self._delete_clients = Mock()

    def check_status(self):
        return True

class TestDataAccessAdapter:
    def test_save_method(self, adapter):
        collection_name = 'clients'
        data = [DataAccessModel(None), DataAccessModel(1)]
        adapter._create_clients.return_value = [data[0]]
        adapter._update_clients.return_value = [data[1]]

        saved_data = adapter.save(collection_name, data)

        assert len(saved_data) == 2
        adapter._create_clients.assert_called_once_with([data[0]])
        adapter._update_clients.assert_called_once_with([data[1]])

    def test_read_method(self, adapter):
        """Test the read method."""
        collection_name = 'clients'
        criteria = DataAccessCriteria()
        adapter._read_clients.return_value = [DataAccessModel(1)]

        result = adapter.read(collection_name, criteria)

        assert len(result) == 1
        adapter._read_clients.assert_called_once_with(criteria)

    def test_delete_method(self, adapter):
        """Test the delete method."""
        collection_name = 'clients'
        criteria = DataAccessCriteria()
        adapter._delete_clients.return_value = [DataAccessModel(1)]

        result = adapter.delete(collection_name, criteria)

        assert len(result) == 1
        adapter._delete_clients.assert_called_once_with(criteria)

    def test_attribute_error_handling(self, adapter):
        """Test handling of AttributeError for unimplemented methods."""
        collection_name = 'non_existent_collection'
        criteria = DataAccessCriteria()
        with patch.object(adapter._logger, 'warning') as mock_logger_warning:
            result = adapter.read(collection_name, criteria)

        assert result == []
        mock_logger_warning.assert_called_once()
        assert 'not implemented' in mock_logger_warning.call_args[0][0]

    def test_unknown_errors_are_raised(self, adapter):
        error_message = 'Some other attribute not found'
        adapter._read_clients.side_effect = AttributeError(error_message)
        collection_name = 'clients'
        criteria = DataAccessCriteria()

        with raises(Exception, match=error_message) as _e:
            adapter.read(collection_name, criteria)

    @fixture
    def adapter(self):
        return ConcreteDataAccessAdapter({"adapters": {"mock": Mock()}}, Mock())
