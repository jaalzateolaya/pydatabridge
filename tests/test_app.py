"""Test module."""

from unittest.mock import Mock, patch
import pytest

from src.pydatabridge.app import App


# pylint: disable=protected-access
class TestApp:
    """Test the application creation and configuration"""

    @pytest.fixture(autouse=True)
    def clean_app_instance(self):
        """Setup test."""
        if hasattr(App, '_instance'):
            delattr(App, '_instance')

    @pytest.fixture
    def app_default_params(self):
        """Default app params fixture.

        :request: TODO
        :returns: App

        """
        dependency_sorter_attrs = {'sort.side_effect': lambda s: s}
        return {
            'logger_provider': Mock(),
            'dependency_sorter': Mock(**dependency_sorter_attrs)
        }

    @pytest.fixture
    def app(self, app_default_params):
        """App fixture provider."""
        return App(**app_default_params)

    def test_default_dry_run(self, app):
        """When the dry_run parameter is not provided, get_is_dry_run should
        return False."""

        is_dry_run = app.get_is_dry_run()

        assert isinstance(is_dry_run, bool)
        assert not is_dry_run

    def test_singleton_pattern(self, app_default_params):
        """No new instances are created after the first intatiation."""

        # Both app1 and app2 should refer to the same instance (Singleton pattern)
        app1 = App(**app_default_params)
        app2 = App(**app_default_params)

        assert app1 is app2

    def test_explicit_dry_run_true(self, app_default_params):
        """When the dry_run parameter is provided and set to True,
        get_is_dry_run should return True."""
        app_params = {**app_default_params, **{'dry_run': True}}
        app = App(**app_params)

        is_dry_run = app.get_is_dry_run()

        assert isinstance(is_dry_run, bool)
        assert is_dry_run

    @patch('src.pydatabridge.app.CommandQueueInvoker')
    def test_run_executes_commands(self, mock_invoker, app_default_params):
        """Tests commands execution after run.

        """
        app = App(**app_default_params)
        app.set_adapters([Mock(), Mock(), Mock()])
        app.set_strategies([Mock(supports_dry_run=True)])

        result = app.run()

        mock_invoker.return_value.execute_commands.assert_called_once()
        assert result == 0

    @patch('src.pydatabridge.app.CommandQueueInvoker')
    def test_run_handles_exception(self, mock_invoker, app_default_params):
        """Test exception handling is performed by the application.

        """
        mock_invoker.return_value.execute_commands.side_effect = Exception("Test Exception")
        app = App(**app_default_params)

        app.set_adapters([Mock(), Mock(), Mock()])
        app.set_strategies([Mock(supports_dry_run=True)])

        result = app.run()

        assert result == 1

    @patch('src.pydatabridge.app.App._populate_command_queue')
    def test_app_warns_dry_run_mode(self, mock_populate_command_queue, app):
        """Test dry run is logged as warning.

        """
        mock_populate_command_queue.return_value = None

        app._is_dry_run = True

        app.run()

        app._logger.warning.assert_called_with('dry_run mode ON.')

    def test_app_warns_not_enough_adapters(self, app):
        """Test the app warns when not enough adapters.

        """
        app.set_adapters([Mock()])
        app._populate_command_queue()

        app._logger.warning.assert_called_with('Nothing to do. Not enough adapters.')

    def test_app_warns_no_strategies(self, app):
        """Test app warns when no strategies.

        """
        app.set_adapters([Mock(), Mock()])
        app._populate_command_queue()

        app._logger.warning.assert_called_with('Nothing to do. There are not strategies.')

    def test_app_warns_strategy_not_support_dry_run(self, app):
        """Test app warns when strategy does not support dry run when dry run
        mode is on.

        """
        strategy = Mock(supports_dry_run=False)

        app.set_adapters([Mock(), Mock()])
        app.set_strategies([strategy])

        app._is_dry_run = True
        app._populate_command_queue()

        app._logger.warning.assert_called_with(
            'Dry-run not supported by %s',
            type(strategy).__name__
        )
