"""Sync strategy."""

from .data_push_strategy import DataPushStrategy


class DataPullStrategy(DataPushStrategy):
    """Data pull syncrhonization strategy.
    """
    def _prepare_adapters(self) -> None:
        self._source_adapters = self._adapters[1:]
        self._dest_adapters = [self._adapters[0]]
