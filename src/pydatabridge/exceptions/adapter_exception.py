"""Exceptions module."""

class AdapterException(Exception):
    """Exceptions thrown by adapters."""
